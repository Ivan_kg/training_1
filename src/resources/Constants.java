package resources;

public class Constants {

	public static final String PAGE_TITLE = "Selenium Framework | Practiceform";
	public static final String PAGE_TITLE_VERIFICATION_MSG = "Verification for main page title is not success";
	public static final String TEXTAREA_VERIFICATION_MSG = "Verification for textarea text is not success ";
	public static final String TEXTAREA_DEFAULT_TEXT = "This is a text area";
	public static final String TEXTAREA_NEW_TEXT = "Hello world";
	public static final String TEXT_BOX_VERIFICATION_MSG = "Verification for text is not success";
	public static final String TETX_BOX_DEFAULT = "This is a text box";
	public static final String VERIFICATION_ERROR_MSG_NOT_SUCCESS = "Verification for input field error messages is not success";
	public static final String SUBMIT_ERROR_MSG = "Please enter at least 2 characters.";
	public static final String VALUE = "2";
	public static final String ATTRIBUTE_STYLE = "style";
	public static final String BUTTON_2_VERIFICATION_MSG = "Verification for button 2 text color is not success";
	public static final String BUTTON_2_STYLE = "background-color: DarkGreen; color: orange;";
	public static final String VERIFICATION_NEW_PAGE_MSG = "Verification for new page URL is not succes";
	public static final String NEW_WINDOW_URL = "http://www.seleniumframework.com/";
	public static final String VERIFICATION_NEW_MESSAGES = "Verification for new window messages text is not success";
	public static final String NEW_WIN_MSG = "This message window is only for viewing purposes";
	public static final String NEW_PAGE_VERIFICATION_MSG = "Verification for new page title is not success";
	public static final String NEW_PAGE_TITLE = "Selenium Framework | Selenium, Cucumber, Ruby, Java et al.";
	public static final String SUBSCRIBE_APROVE_VERIFICATION_MSG = "Verification for subscribe aprove messages is not success";
	public static final String SUBSCRIBE_MESSAGES = "�will receive a verification message once you submit this form. FeedBurner activates your subscription to �Selenium Framework� once you respond to this verification message.";
	public static final String SUBSCRIBE_EMAIL = "ivantest@test.com";
	public static final String CHECKBOX_OPTION_1 = "Option 1";
	public static final String RADIO_BTN_OPTION_2 = "Option 2";
}

