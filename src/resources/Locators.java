package resources;

public class Locators {

	public static final String TEXTAREA = "//textarea[contains(text(),'This is a text area')]";
	public static final String TEXT_BOX = "//input[contains(@value,'This is a text box')]";
	public static final String SUBMIT = "//input[@name='vfb-submit']";
	public static final String VERIFICATION_INPUT = "//span[@class='vfb-span']/input[contains(@style,'display:block')]";
	public static final String SUBMIT_ERROR_MSG = "//span[@class='vfb-span']/label[contains(text(),'Please enter at least 2 characters.')]";
	public static final String BUTTON_2 = "//button[@id='doubleClick']";
	public static final String DRAG_ME = "//button[@id='draga']";
	public static final String DRAG_TO = "//button[@id='dragb']";
	public static final String BUTTON_NEW_BROWSER_WINDOW = "//button[@id='button1']";
	public static final String BUTTON_NEW_WINDOW_MESSAGES = "//button[@onclick='newMsgWin()']";
	public static final String NEW_WINDOW_MESSAGES_BODY = "//body[contains(text(),'This message window is only for viewing purposes')]";
	public static final String BUTTON_NEW_BROWSER_TAB = "//button[@onclick='newBrwTab()']";
	public static final String EMAIL_SUBSCRIBE = "//input[@name='email'][not(contains(@placeholder,'E-mail'))]";
	public static final String SUBMIT_SUBSCRIBE = "//input[@value='Subscribe']";
	public static final String SUBSCRIBE_APPROVE_MSG = "//form[@name='emailSyndicationVerificationForm']/p[contains(text(),'will receive a verification message')]";
	public static final String CHECKBOXES="//input[@type='checkbox']";
	public static final String RADIO_BTN="//input[@type='radio']";
}
