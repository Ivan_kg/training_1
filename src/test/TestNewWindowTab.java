package test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import resources.Constants;
import common.MainPage;
import dialogs.NewBrowserTab;
import base.Base;

public class TestNewWindowTab extends Base {
	final org.apache.log4j.Logger logger = org.apache.log4j.Logger
			.getLogger(TestNewWindowTab.class);

	@Before
	public void setUp() {
		fireFoxBrowser();
		setProperty();
		open("http://www.seleniumframework.com/Practiceform/");
	}

	/**
	 * This test will navigate to button, press button and go to new window Tab
	 * , after switching test will verifies new page title, compare expected
	 * title with current page title
	 */
	@Test
	public void switchToWindowTab() {
		MainPage newTab = new MainPage();
		NewBrowserTab tabSwitch = newTab.goToNewBrowserTab(driver);
		tabSwitch.newPageTitle(driver, Constants.NEW_PAGE_TITLE);
		logger.info("Checked, switch to Window tab and verifies page title");
	}

	@After
	public void thearDown() {
		quit();
	}

}

