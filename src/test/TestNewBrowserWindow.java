package test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import resources.Constants;
import common.MainPage;
import dialogs.NewBrowserWindow;
import base.Base;

public class TestNewBrowserWindow extends Base {
	final org.apache.log4j.Logger logger = org.apache.log4j.Logger
			.getLogger(TestNewBrowserWindow.class);

	@Before
	public void setUp() {
		fireFoxBrowser();
		setProperty();
		open("http://www.seleniumframework.com/Practiceform/");
	}

	/**
	 * This test will navigate to button, press button1 and go to new browser
	 * window, after switching test will verifies new page URL, compare expected
	 * page URL with current page URL
	 */
	@Test
	public void switchToNewWindow() {
		MainPage newWin = new MainPage();
		NewBrowserWindow winSwitch = newWin.goToNewBrowserWindow(driver);
		winSwitch.pageURL(driver, Constants.NEW_WINDOW_URL);
		logger.info("Checked, switch to the new window amd verifies page url");
	}

	@After
	public void thearDown() {
		quit();
	}

}
