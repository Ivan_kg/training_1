package test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import resources.Constants;
import common.MainPage;
import base.Base;

public class TestMainPage extends Base {
	final org.apache.log4j.Logger logger = org.apache.log4j.Logger
			.getLogger(TestMainPage.class);

	@Before
	public void setUp() {
		fireFoxBrowser();
		setProperty();
		open("http://www.seleniumframework.com/Practiceform/");
	}

	/**
	 * This test will compare the expected title with the current page title
	 */
	@Test
	public void pageTitle() {
		MainPage title = new MainPage();
		title.pageTitle(driver, Constants.PAGE_TITLE);
		logger.info("Checked page title");
	}

	/**
	 * This test will compare the expected text with the current text on
	 * textarea field
	 */
	@Test
	public void textareaDefault() {
		MainPage textarea = new MainPage();
		textarea.textareaVerifies(driver, Constants.TEXTAREA_DEFAULT_TEXT);
		logger.info("Checked textarea default");
	}

	/**
	 * This test will navigate to textarea, clear current text and set new text
	 */
	@Test
	public void textareaNewText() {
		MainPage textarea = new MainPage();
		textarea.textareaClear(driver);
		textarea.textareaNewText(driver, Constants.TEXTAREA_NEW_TEXT);
		logger.info("Checked textarea, clear text and set new text");
	}

	/**
	 * This test will navigate to text box and verifies default text, test will
	 * compare expected text with current text
	 */
	@Test
	public void textBoxVerifies() {
		MainPage textBox = new MainPage();
		textBox.textBoxVerifies(driver, Constants.TETX_BOX_DEFAULT);
		logger.info("Checked textbox, default");
	}

	/**
	 * This test will navigate to verification input field, set value, press
	 * submit button and verifies error messages
	 */
	@Test
	public void submitErrorMessage() {
		MainPage submit = new MainPage();
		submit.verificationInput(driver, Constants.VALUE);
		submit.clickSubmit(driver);
		submit.submitErrorMsg(driver, Constants.SUBMIT_ERROR_MSG);
		logger.info("Checked submit error message");
	}

	/**
	 * This test will navigate and provide doubleClick on button2, after this
	 * button text must change color and test will comapre expected with current
	 * color
	 */
	@Test
	public void button2() {
		MainPage button = new MainPage();
		button.doubleClick(driver);
		button.button2Color(driver, Constants.BUTTON_2_STYLE);
		logger.info("Checked button2, double click and text color verifies");
	}

	/**
	 * This test will navigate to drag button, click, hold, move and drop
	 * 
	 */
	@Test
	public void dragAndDrop() throws InterruptedException {
		MainPage dragDrop = new MainPage();
		dragDrop.dragAndDrop(driver);
		logger.info("Checked drag and drop action");
	}

	/**
	 * This test will navigate to checkboxes, count number of checkboxes are
	 * presentes , start the loop from first to last checkbox, Select the
	 * checkbox it the value of the checkbox is same what we sent
	 */
	@Test
	public void checkboxes() {
		MainPage checkbox = new MainPage();
		checkbox.checkboxes(driver, Constants.CHECKBOX_OPTION_1);
		logger.info("Checked checkboxes");
	}

	/**
	 * This test will navigate to radio button, count number of radio buttons
	 * are presentes , start the loop from first to last radio button, Select
	 * the radio button it the value of the radio button is same what we sent
	 */
	@Test
	public void radioBtn() {
		MainPage radioBtn = new MainPage();
		radioBtn.radioButtons(driver, Constants.RADIO_BTN_OPTION_2);
		logger.info("Chekced radio Buttons");
	}

	@After
	public void thearDown() {
		quit();
	}

}

