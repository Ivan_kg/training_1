package test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import base.Base;
import resources.Constants;
import common.MainPage;
import dialogs.SubscribeWindow;

public class TestSubscribeWindow extends Base {

	final org.apache.log4j.Logger logger = org.apache.log4j.Logger
			.getLogger(TestSubscribeWindow.class);

	@Before
	public void setUp() {
		fireFoxBrowser();
		setProperty();
		open("http://www.seleniumframework.com/Practiceform/");

	}

	/**
	 * This test will navigate to subscribe form set email, press subscribe and
	 * go to new subscribe window , after switching test will verifies subscribe
	 * message, compare expected text with current text
	 */
	@Test
	public void subscribeAproved() {

		MainPage subscribe = new MainPage();
		subscribe.emailSubscribe(driver, Constants.SUBSCRIBE_EMAIL);
		SubscribeWindow subscribeAprove = subscribe.goToSubscribeWindow(driver);
		subscribeAprove.subscribe(driver, Constants.SUBSCRIBE_MESSAGES);
		logger.info("Checked, subscribe aprove");

	}

	@After
	public void thearDown() {
		quit();
	}

}
