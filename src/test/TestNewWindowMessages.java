package test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import resources.Constants;
import common.MainPage;
import dialogs.NewWindowMessages;
import base.Base;

public class TestNewWindowMessages extends Base {
	final org.apache.log4j.Logger logger = org.apache.log4j.Logger
			.getLogger(TestNewWindowMessages.class);

	@Before
	public void setUp() {
		fireFoxBrowser();
		setProperty();
		open("http://www.seleniumframework.com/Practiceform/");
	}

	/**
	 * This test will navigate to button, press button and go to new messages
	 * window , after switching test will verifies defautl window messages,
	 * compare expected text with current text
	 */
	@Test
	public void switchToNewWindowMsg() {
		MainPage newWinMsg = new MainPage();
		NewWindowMessages winSwitch = newWinMsg.goToNewWindowMessages(driver);
		winSwitch.messagesText(driver, Constants.NEW_WIN_MSG);
		logger.info("Checked, switch to the new window message, and verifies default msg");
	}

	@After
	public void thearDown() {
		quit();
	}

}

