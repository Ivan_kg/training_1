package base;

import java.util.concurrent.TimeUnit;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public abstract class Base {

	public static WebDriver driver;

	public WebDriver getDriver() {
		return driver;
	}

	public void fireFoxBrowser() {
		driver = new FirefoxDriver();
	}

	public void setProperty() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		DOMConfigurator.configure("log4j.xml");

	}

	public void open(String url) {
		getDriver().get(url);
	}

	protected void quit() {
		driver.quit();
	}

}
