package dialogs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import resources.Constants;
import resources.Locators;

public class SubscribeWindow extends Locators {

	/**
	 * This method verifies Subscribe Aprove messages
	 * 
	 * @param driver
	 * @param expectedMsg
	 *            String
	 * @author imarjanovic
	 */
	public void subscribe(WebDriver driver,
			String expectedMsg) {
		String actual = driver.findElement(By.xpath(SUBSCRIBE_APPROVE_MSG))
				.getText();
		Assert.assertEquals(actual, expectedMsg,
				Constants.SUBSCRIBE_APROVE_VERIFICATION_MSG);
	}

}
