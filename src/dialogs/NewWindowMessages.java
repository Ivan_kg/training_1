package dialogs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import resources.Constants;
import resources.Locators;

public class NewWindowMessages extends Locators {

	/**
	 * This method verifies new window messages text
	 * 
	 * @param driver
	 * @param expectedText
	 *            String
	 * @author imarjanovic
	 */
	public void messagesText(WebDriver driver, String expectedText) {
		String actual = driver.findElement(By.xpath(NEW_WINDOW_MESSAGES_BODY))
				.getText();
		Assert.assertEquals(actual, expectedText,
				Constants.VERIFICATION_NEW_MESSAGES);
	}

}
