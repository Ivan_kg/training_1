package dialogs;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import resources.Constants;

public class NewBrowserTab {

	/**
	 * This method verifies title of new browser page
	 * 
	 * @param driver
	 * @param expectedTitle
	 *            String
	 * @author imarjanovic
	 */
	public void newPageTitle(WebDriver driver, String expectedTitle) {
		String actual = driver.getTitle();
		Assert.assertEquals(actual, expectedTitle,
				Constants.NEW_PAGE_VERIFICATION_MSG);
	}

}
