package dialogs;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import resources.Constants;

public class NewBrowserWindow {

	/**
	 * This method verifies page URL
	 * 
	 * @param driver
	 * @param expectedURL
	 *            String
	 * @author imarjanovic
	 */
	public void pageURL(WebDriver driver, String expectedURL) {
		String actual = driver.getCurrentUrl();
		Assert.assertEquals(actual, expectedURL,
				Constants.VERIFICATION_NEW_PAGE_MSG);
	}

}

