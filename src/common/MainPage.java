package common;

import static org.testng.AssertJUnit.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import dialogs.NewBrowserTab;
import dialogs.NewBrowserWindow;
import dialogs.NewWindowMessages;
import dialogs.SubscribeWindow;
import resources.Constants;
import resources.Locators;

public class MainPage extends Locators {

	/**
	 * This method will verifies title of main page
	 * 
	 * @param driver
	 * @param expectedTitle
	 *            String
	 * @author imarjanovic
	 */
	public void pageTitle(WebDriver driver, String expectedTitle) {
		String actual = driver.getTitle();
		assertEquals(Constants.PAGE_TITLE_VERIFICATION_MSG, expectedTitle,
				actual);
	}

	/**
	 * This method should navigate to textarea and verifies text
	 * 
	 * @param driver
	 * @param expectedText
	 *            String
	 * @author imarjanovic
	 */
	public void textareaVerifies(WebDriver driver, String expectedText) {
		String actual = driver.findElement(By.xpath(TEXTAREA)).getText();
		assertEquals(Constants.TEXTAREA_VERIFICATION_MSG, expectedText, actual);
	}

	/**
	 * This method should navigate to textarea and clear text
	 * 
	 * @param driver
	 * @author imarjanovic
	 */
	public void textareaClear(WebDriver driver) {
		driver.findElement(By.xpath(TEXTAREA)).clear();
	}

	/**
	 * This method should navigate to textarea and write new text
	 * 
	 * @param driver
	 * @param newText
	 *            String
	 * @author imarjanovic
	 */
	public void textareaNewText(WebDriver driver, String newText) {
		driver.findElement(By.xpath(TEXTAREA)).sendKeys(newText);
	}

	/**
	 * This method should navigate to text box and verifies text
	 * 
	 * @param driver
	 * @param expectedText
	 *            String
	 * @author imarjanovic
	 */
	public void textBoxVerifies(WebDriver driver, String expectedText) {
		String actual = driver.findElement(By.xpath(TEXT_BOX)).getAttribute(
				"value");
		Assert.assertEquals(actual, expectedText,
				Constants.TEXT_BOX_VERIFICATION_MSG);

	}

	/**
	 * This method should navigate to submit and press submit button
	 * 
	 * @param driver
	 * @author imarjanovic
	 */
	public void clickSubmit(WebDriver driver) {
		driver.findElement(By.xpath(SUBMIT)).click();
	}

	/**
	 * This method should navigate to verification field and set value
	 * 
	 * @param driver
	 * @param value
	 *            String
	 * @author imarjanovic
	 */
	public void verificationInput(WebDriver driver, String value) {
		driver.findElement(By.xpath(VERIFICATION_INPUT)).sendKeys(value);
	}

	/**
	 * This method should navigate to verification field and verifies Submit
	 * error messages
	 * 
	 * @param driver
	 * @param errorMsg
	 *            String
	 * @author imarjanovic
	 */
	public void submitErrorMsg(WebDriver driver, String expectedMsg) {
		String actual = driver.findElement(By.xpath(SUBMIT_ERROR_MSG))
				.getText();
		Assert.assertEquals(actual, expectedMsg,
				Constants.VERIFICATION_ERROR_MSG_NOT_SUCCESS);

	}

	/**
	 * This method should navigate to button2 and provide double click action
	 * 
	 * @param driver
	 * @author imarjanovic
	 */
	public void doubleClick(WebDriver driver) {
		WebElement element = driver.findElement(By.xpath(BUTTON_2));
		Actions action = new Actions(driver);
		action.doubleClick(element).build().perform();
		;
	}

	/**
	 * This method should navigate to button2 and verifies button2 text color
	 * 
	 * @param driver
	 * @param style
	 *            String
	 * @author imarjanovic
	 */
	public void button2Color(WebDriver driver, String expectedStyle) {
		String actual = driver.findElement(By.xpath(BUTTON_2)).getAttribute(
				Constants.ATTRIBUTE_STYLE);
		Assert.assertEquals(actual, expectedStyle,
				Constants.BUTTON_2_VERIFICATION_MSG);
	}

	/**
	 * This method should provide drag and drop action
	 * 
	 * @param driver
	 * @author imarjanovic
	 * @throws InterruptedException
	 *             2sec
	 */
	public void dragAndDrop(WebDriver driver) throws InterruptedException {

		WebElement From = driver.findElement(By.xpath(DRAG_ME));
		WebElement To = driver.findElement(By.xpath(DRAG_TO));
		Actions builder = new Actions(driver);
		Action dragAndDrop = builder.clickAndHold(From).moveToElement(To)
				.release(To).build();
		dragAndDrop.perform();
	}

	/**
	 * This method should navigate to subscribe form, and set email
	 * 
	 * @param driver
	 * @param email
	 *            String
	 * @author imarjanovic
	 */
	public void emailSubscribe(WebDriver driver, String email) {
		driver.findElement(By.xpath(EMAIL_SUBSCRIBE)).sendKeys(email);
	}

	/**
	 * This method should navigate to subscribe button, click and go to the new
	 * Subscribe window
	 * 
	 * @param driver
	 * @return new Subscribe Window
	 * @author imarjanovic
	 */
	public SubscribeWindow goToSubscribeWindow(WebDriver driver) {
		driver.findElement(By.xpath(SUBMIT_SUBSCRIBE)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		return new SubscribeWindow();
	}

	/**
	 * This method should navigate to button1(new browser window), press button1
	 * and go to new browser window
	 * 
	 * @param driver
	 * @return new Browser Window
	 * @author imarjanovic
	 */
	public NewBrowserWindow goToNewBrowserWindow(WebDriver driver) {
		driver.findElement(By.xpath(BUTTON_NEW_BROWSER_WINDOW)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		return new NewBrowserWindow();
	}

	/**
	 * This method should navigate to button(new window messages), press button
	 * and go to new window messages
	 * 
	 * @param driver
	 * @return new Window Messages
	 * @author imarjanovic
	 */
	public NewWindowMessages goToNewWindowMessages(WebDriver driver) {
		driver.findElement(By.xpath(BUTTON_NEW_WINDOW_MESSAGES)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		return new NewWindowMessages();
	}

	/**
	 * This method should navigate to button(new browser tab), press button and
	 * go to new browser tab
	 * 
	 * @param driver
	 * @return NewBrowserTab
	 * @author imarjanovic
	 */
	public NewBrowserTab goToNewBrowserTab(WebDriver driver) {
		driver.findElement(By.xpath(BUTTON_NEW_BROWSER_TAB)).click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		return new NewBrowserTab();
	}

	/**
	 * This method should navigate to checkboxes, count number of checkboxes are
	 * presentes , start the loop from first to last checkbox, Select the
	 * checkbox it the value of the checkbox is same what we sent
	 * 
	 * @param driver
	 * @param checkboxValue
	 *            String
	 * @author imarjanovic
	 */
	public void checkboxes(WebDriver driver, String checkboxValue) {
		List checkBox = driver.findElements(By.xpath(CHECKBOXES));
		int iSize = checkBox.size();
		for (int i = 0; i < iSize; i++) {

			String sValue = ((WebElement) checkBox.get(i))
					.getAttribute("value");
			if (sValue.equalsIgnoreCase(checkboxValue)) {
				((WebElement) checkBox.get(i)).click();
				break;
			}
		}
	}

	/**
	 * This method should navigate to radio buttons, count number of radio
	 * buttons are presentes , start the loop from first to last radio button,
	 * Select the radiobutton it the value of the radiobutton is same what we
	 * sent
	 * 
	 * @param driver
	 * @param radioBtnValue
	 *            String
	 */
	public void radioButtons(WebDriver driver, String radioBtnValue) {
		List radioBtn = driver.findElements(By.xpath(RADIO_BTN));
		int iSize = radioBtn.size();
		for (int i = 0; i < iSize; i++) {

			String sValue = ((WebElement) radioBtn.get(i))
					.getAttribute("value");
			if (sValue.equalsIgnoreCase(radioBtnValue)) {
				((WebElement) radioBtn.get(i)).click();
				break;
			}
		}
	}

}

